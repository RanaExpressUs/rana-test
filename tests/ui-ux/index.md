# Seja bem vindo - UI/UX

A Rana Express é uma empresa do segmento financeiro que atua nos Estados Unidos com a comunidade brasileira. A nossa principal missão é tornar o uso das nossas ferramentas um local confortável e que seja capaz de fazer usuário se aproximar suas origens. 

O nosso processo seletivo passa pelas seguintes etapas:

1. Primeiro contato com RH ou recrutador;
2. Primeira entrevista;
3. Análise de portfólio, currículo e outras experiências;
4. Teste prático;
5. Entrevista final.

## Teste prático

O nosso principal produto é captar o dinheiro de pessoas fora do país e creditar em contas dentro do Brasil, esse processo é denominado remessa ou ordem. Esses envios podem ser feito via app ou em um agente físico credenciado pela Rana.
Quem está enviando dinheiro é donominado "SENDER", E quem recebe o dinheiro aqui no Brasil é chamado "Receiver" ou simplesmente beneficiário.
O agente é um local específico onde atendentes são credenciados para executar as transações de dinheiro via plataforma Web. Pode ser uma loja da Rana ou um supermercado, ou padaria ou qualquer comércio que tem um fluxo considerável da comunidade brasileira residente no país.

- Algumas características do Sender:
    - Necessita ter um endereço e telefone Americano obrigatoriamente.
    - Necessita ter comprovante de residência em um dos estados atendido pela Rana.
    - Necessita ter mais de 18 anos.
    - Necessita anexar documentos necessários para comprovação.
    

- Algumas características do Reciver:
    - Necessita ter um endereço brasileiro.
    - É obrigatório o número do CPF.
    - Necessita ter uma conta bancária vinculada para receber o dinheiro.
    - Necessita ser declarado qual a relação com o Sender.

- Algumas características da ordem/remessa:
    - Necessita de um valor em dólar ou real.
    - Necessita de um simulador de câmbio que permita entrar com valores nas duas moedas e a suas respectivas conversões e descontos de taxas.
    - Necessita de um motivo.
        - Ex: Tratamento de saude, ajuda familiar e etc.

# Ordens pelo App

As ordens pelo app são enviadas pelo próprio Sender em que ele é responsável pela inserção dos dados do Reciver, valores, documentos de compliance, motivo e pagamento.

# Ordens em um agent

As ordens no agente são realizadas por uma pessoa credenciada que é responsável por preencher todo o fluxo da ordem no sistema web. Um banco de dados é consultado para checagem das informações do Sender, Receiver, contas, entre outros. Caso esses dados já existam nas bases de dados da Rana o a gente faz apenas o preenchimento das informações e enviar ordem para ser paga. Caso algum dado não exista ou a gente é direcionado para a tela de cadastro desta entidade específica.


# Desafio

1. Criar uma tela de cadastros de remessa PARA O AGENTE que contenha:
    - Local para busca e inserção do Sender por nome, telefone ou documento.
    - Local para busca e inserção do Reciver por nome telefone ou documento.
    - Local que redirecione o agente para tela de cadastrar SENDER OU RECEIVER caso eles não estejam cadastrados.
    - Local para inserção de motivo da remessa.
    - Local para inserção de conta de destino do Reciver.
    - Local para inserção de uma nova conta caso a conta não esteja cadastrada para este Reciver.
    - Local para Envio de documentos do Senter.
    - Local para simulação dos valores.
        - Bandeiras do Brasil e dos Estados Unidos deverão ser usadas no simulador.
        - A inserção de moeda e valores devem possibilitar a inserção tanto em real quanto em dólar.
    - Botão de envio.

2. A tela deverá ser projetada em um software de desenho de interface, sugerimos o Figma.



