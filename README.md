![rana-logo](./assets/logo/logo.png)

# Rana - Seleção

## Index
[A Rana Express](#a-rana-express)<br>
[O Projeto](#o-projeto)<br>
[Logos](#logos)<br>
[Cores](#cores)<br>



# A Rana Express

Rana Financial LLC é uma empresa de transferência de dinheiro entre os Estados Unidos e o Brasil. Iniciamos nossas operações em Marietta (GA) e, em julho de 2010, transferimos nossa sede para Pompano Beach (FL). 

Conhecemos as necessidades da comunidade brasileira na América e focamos nossos esforços em facilitar e agilizar a transferência de dinheiro para seu país de origem, onde nossos clientes possuem família, amigos, dependentes e parceiros de negócio.

Site: 
> https://ranaexpress.com/pt_br/

# O Projeto

Rana express é uma **empresa americana**, ela precisa que seus **clientes** possam **enviar dinheiro** para **beneficiários brasileiros** em **bancos e contas** do brasil.
O cliente precisa ter acesso a uma **cotação dólar X real** no momento do **fechamento de câmbio**. 
Ele fará o **depósito** do dólar por meio de **ACH** (uma espécie de TED brasileiro), fornecido por um **Gateway de pagamento**, seja banco ou fintech. Além desse, pode depositar **fisicamente** em dinheiro nos **agentes** autorizados.

Essa remessa (envio) funciona como um serviço de câmbio.

Importante que para que funcione corretamente tanto o beneficiário como o cliente precisam cumprir algumas regras, que chamamos de Regras de COMPLIANCE. Essas regras definem totais (valores) que podem ser enviados em determinados espaços de tempo (dias); com base nessas duas condições ele precisa cumprir com alguns requisitos, geralmente esses requisitos são anexar documentos e/ou responder por perguntas e/ou informações por escrito.

Caso o cliente caia nesse regra, será necessário a intervenção do mesmo para entregar esse requisitos e em seguida precisa ser aprovado por um usuário autorizado da área de COMPLIANCE para liberar o pagamento.

Quando um pagamento se enquadrar nessas regras de COMPLIANCE precisam ser guardadas em registros, todas essas informações e histórico da mesma, para quando necessário, os fiscais possam ter acesso e saber os detalhes que a ordem (remessa) entrou no COMPLIANCE, que foi feito, quando foi feito e qual usuário autorizou.

Todos esses envios (remessas) precisam ter uma natureza de operação, informando o motivo que esta enviando esse dinheiro, no inicio da operação.

Dado por completo o serviço de câmbio de acordo com regras de compliance o a remessa terá seu status como em pagamento, e será enviada para intermediador parceiro (banco) que será responsável por pegar essas informações da remessa, e entregar o valor de moeda de destino, nesse caso o Real para a conta do beneficiário.

Num cenário de negócios ideal, seria importante esses usuários terem uma conta, como uma conta corrente, com a possibilidade de cada conta ser em uma moeda diferente.

Ele poderia fazer depósitos nessa conta usando o mesmo serviço de ACH ou em dinheiro no agente, ou qualquer outro meio de pagamento.

E deve usar esse dinheiro em conta para fazer suas remessas para beneficiários, ou para qualquer outra finalidade, pagar um serviço, um produto, uma compra etc. 

Todas essas movimentações devem ser guardados e exibidas em seus extratos de conta (balance) para que ele possa consultar.

# Logos
- Acesse a pasta assets para acesso as logos em PNG:

-Logo
<h1 align="center">
    <img alt="Logo" title="#logo" src="assets/logo/logo.png" width="200px" />
</h1>
-Logo White
<h1 align="center">
    <img alt="Logo" title="#logo" src="assets/logo/logo_white.png" width="200px" />
</h1>

-Icone
<h1 align="center">
    <img alt="Icone" title="#icone" src="assets/logo/icon.png" width="80px" />
</h1>

# Cores
- azul_rana = '#1c6aa5'
- azul_escuro = '#000729'
- azul_claro = '#238ead'
- azul_aqua = '#d2ebf7'
- azul_aqua_extra = '#f0faff'